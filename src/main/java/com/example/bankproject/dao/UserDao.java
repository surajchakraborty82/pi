package com.example.bankproject.dao;


import java.util.List;

import com.example.bankproject.response.UserDepartmentResponse;
public interface UserDao {

	public List<UserDepartmentResponse> getUserDepartment();
	public List<UserDepartmentResponse> getLatestTransactions();


}
